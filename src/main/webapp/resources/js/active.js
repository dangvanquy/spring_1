(function ($) {
    'use strict';

    var browserWindow = $(window);
    var treadingPost = $('.treading-post-area');

    // :: 1.0 Preloader Active Code
    browserWindow.on('load', function () {
        $('.preloader').fadeOut('slow', function () {
            $(this).remove();
        });
    });

    // :: 2.0 Nav Active Code
    if ($.fn.classyNav) {
        $('#buenoNav').classyNav();
    }

    // :: 3.0 Sticky Active Code
    if ($.fn.sticky) {
        $("#sticker").sticky({
            topSpacing: 0
        });
    }

    // :: 4.0 niceSelect Active Code
    if ($.fn.niceSelect) {
        $("select").niceSelect();
    }

    // :: 5.0 Video Active Code
    if ($.fn.magnificPopup) {
        $('.img-zoom').magnificPopup({
            type: 'image',
            gallery: {
                enabled: true
            }
        });
    }

    // :: 6.0 Sliders Active Code
    if ($.fn.owlCarousel) {

        var welcomeSlide = $('.hero-post-slides');
        var videoSlides = $('.video-slides');
        var albumSlides = $('.albums-slideshow');

        welcomeSlide.owlCarousel({
            items: 3,
            margin: 30,
            loop: true,
            nav: true,
            navText: ['Prev', 'Next'],
            dots: false,
            autoplay: true,
            center: true,
            autoplayTimeout: 7000,
            smartSpeed: 1000,
            responsive: {
                0: {
                    items: 1
                },
                768: {
                    items: 2
                },
                992: {
                    items: 3
                }
            }
        });

        welcomeSlide.on('translate.owl.carousel', function () {
            var slideLayer = $("[data-animation]");
            slideLayer.each(function () {
                var anim_name = $(this).data('animation');
                $(this).removeClass('animated ' + anim_name).css('opacity', '0');
            });
        });

        welcomeSlide.on('translated.owl.carousel', function () {
            var slideLayer = welcomeSlide.find('.owl-item.active').find("[data-animation]");
            slideLayer.each(function () {
                var anim_name = $(this).data('animation');
                $(this).addClass('animated ' + anim_name).css('opacity', '1');
            });
        });

        $("[data-delay]").each(function () {
            var anim_del = $(this).data('delay');
            $(this).css('animation-delay', anim_del);
        });

        $("[data-duration]").each(function () {
            var anim_dur = $(this).data('duration');
            $(this).css('animation-duration', anim_dur);
        });
    }

    // :: 7.0 ScrollUp Active Code
    if ($.fn.scrollUp) {
        browserWindow.scrollUp({
            scrollSpeed: 1500,
            scrollText: '<i class="fa fa-angle-up"></i>'
        });
    }

    // :: 8.0 Tooltip Active Code
    if ($.fn.tooltip) {
        $('[data-toggle="tooltip"]').tooltip()
    }

    // :: 9.0 Prevent Default a Click
    $('a[href="#"]').on('click', function ($) {
        $.preventDefault();
    });

    // :: 10.0 Wow Active Code
    if (browserWindow.width() > 767) {
        new WOW().init();
    }

    // :: 11.0 niceScroll Active Code
    if ($.fn.niceScroll) {
        $("#treadingPost").niceScroll();
    }

    // :: 12.0 Toggler Active Code
    $('#toggler').on('click', function () {
        treadingPost.toggleClass('on');
    });
    $('.close-icon').on('click', function () {
        treadingPost.removeClass('on');
    });
    
    $(".cart").click(function() {
    	var name = $("#name").text();
    	var id = $("#name").attr("data-id");
    	var iddetail = $(this).closest("tr").find(".color").attr("data-iddetail");
    	var price = $("#price").attr("data-price");
    	var idcolor = $(this).closest("tr").find(".color").attr("data-color");
    	var namecolor = $(this).closest("tr").find(".color").text();
    	location.reload();
    	$.ajax({
    		url : "/spring/base/cart",
    		type : 	"GET",
    		data : {
    			id : id,
    			iddetail : iddetail,
    			name : name,
    			price : price,
    			namecolor : namecolor,
    			idcolor : idcolor,
    			quantity : 1
    		},
    		
    		success : function(value) {
    			
    		}
    		
    	})
    });
    
    $(".delete").click(function() {
    	var self = $(this);
    	var id = $(this).closest("tr").find(".name").attr("data-id");
    	var idcolor = $(this).closest("tr").find(".color").attr("data-color");
    	location.reload();
    	$.ajax({
    		url : "/spring/base/delete",
    		type : "GET",
    		data : {
    			id : id,
    			idcolor : idcolor
    		},
    		
    		success : function(value) {
    			self.closest("tr").remove();
    		}
    	})
    });
    
    totalPriceCart();
    
    function totalPriceCart() {
    	var totalprice = 0;
    	$(".price").each(function() {
    		var giatien = $(this).text();
    		var quantity = $(this).closest("tr").find(".quantity").val();
    		
    		var format = parseFloat(giatien).toFixed(6).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.").toString();
    		
    		$(this).html(format);
    		
    		var total = quantity * parseFloat(giatien);
    		totalprice = totalprice + total;

    		var formattotalprice = totalprice.toFixed(6).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.").toString();
    		$("#totalprice").html(formattotalprice + " VNĐ");
    	})
    }
    
    $(".quantity").change(function(){
    	var quantity = $(this).val();
    	totalPriceCart();
    	
    	var id = $(this).closest("tr").find(".name").attr("data-id");
		var idcolor = $(this).closest("tr").find(".color").attr("data-color");
        
    	$.ajax({
    		url : "/spring/base/updatequantity",
    		type : "GET",
    		data : {
    			id : id,
    			idcolor : idcolor,
    			quantity : quantity,
    		},
    		
    		success : function(value) {
    			
    		}
    	
    	});
    	});
    

})(jQuery);