(function() {
    "use strict";

    // custom scrollbar

    $("html").niceScroll({styler:"fb",cursorcolor:"#1b93e1", cursorwidth: '6', cursorborderradius: '10px', background: '#FFFFFF', spacebarenabled:false, cursorborder: '0',  zindex: '1000'});

    $(".scrollbar1").niceScroll({styler:"fb",cursorcolor:"#1b93e1", cursorwidth: '6', cursorborderradius: '0',autohidemode: 'false', background: '#FFFFFF', spacebarenabled:false, cursorborder: '0'});

	
	
    $(".scrollbar1").getNiceScroll();
    if ($('body').hasClass('scrollbar1-collapsed')) {
        $(".scrollbar1").getNiceScroll().hide();
    }
    
    $("body").on("click", ".paging", function(){
    	$(".paging").removeClass("active");
    	$(this).addClass("active");
		var number = $(this).text();
		var beginNumber = (number - 1) * 5;
		
		$.ajax({
			url : "/spring/base/paging",
			type : "GET",
			data : {
				beginNumber : beginNumber,
			},
		
			success : function (value) {
				var table = $("#table-product").find("tbody");
				table.empty();
				table.append(value);
			}
		});
	});
    
    $("#delete").click(function() {
    	$(".checkSingle").each(function() {
    		if (this.checked) {
    			var id = $(this).val();
    			var self = $(this);
    			
    			$.ajax({
    	    		url : "/spring/base/deleteproduct",
    	    		type : "GET",
    	    		data : {
    	    			id : id,
    	    		},
    	    		
    	    		success : function() {
    	    			self.closest("tr").remove();
    	    		}
    	    	})
    		}
    	})
    	
    	
    })
    
    $("#check-all").change(function() {
    	if (this.checked) {
    		$(".checkSingle").each(function(){
    			this.checked=true;
    		})
    	} else {
    		$(".checkSingle").each(function(){
    			this.checked=false;
    		})
    	}
    })
    
    var files = [];
    var image = {};
    $("#image").change(function(event) {
    	files = event.target.files;
    	var form = new FormData();
    	form.append("files", files[0]);
    	image = files[0].name;
    	$.ajax({
    		url : "/spring/base/uploadfile",
    		type : "POST",
    		data : form,
    		contentType : false,
    		processData : false,
    		enctype : "multipart/form-data",
    		
    		success : function(value) {
    			
    		}
    	})
    })
    
    $("body").on("click","#btn-detail", function(event) {
    	event.preventDefault();
    	var detailclone = $("#detail-product").clone().removeAttr("id");
    	$("#content-detail-product").append(detailclone);
    })
    
    $("#btn-add").click(function(event) {
    	event.preventDefault();
    	var formdata = $("#form-data").serializeArray();
    	var json = {};
    	var detailArray = [];
    	
    	$.each(formdata, function(i,field) {
    		json[field.name] = field.value;
    	})
    	
    	$("#content-detail-product >.detail-product").each(function(){
    		var detailObject = {};
    		var color = $(this).find('select[name="color"]').val();
    		detailObject["color"] = color;
    		detailArray.push(detailObject);
    	})
    	
    	json["detailproduct"] = detailArray;
    	json["image"] = image;
    	console.log(json);
    	$.ajax({
    		url : "/spring/base/addproduct",
    		type : "POST",
    		data : {
    			datajson : JSON.stringify(json),
    		},
    		
    		success : function(value) {
    			
    		}
    	})
    })
    
    $(".btn-update").click(function() {
    	var id = $(this).attr("data-id");
    	
    	$.ajax({
    		url : "/spring/base/getdetailproduct",
    		type : "POST",
    		data : {
    			id : id,
    		},
    		
    		success : function(value) {
    			console.log(value);
    			$("#name").val(value.name);
    			$("#price").val(value.price);
    			$("#description").val(value.description);
    			$("#category").val(value.category.id);
    			$("#image").val(value.image);
    			$("#content-detail-product").html("");
    			for (var i = 0;i<4;i++) {
    				var detailClone = $("#detail-product").clone().removeAttr("id");
    				detailClone.val(value.detailProducts[i].color.id);
    				$("#content-detail-product").append(detailClone);
    			}
    		} 
    	})
    })

})(jQuery);

                     
     
  