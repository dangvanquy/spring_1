<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>detail</title>

<link rel="stylesheet" href="resources/css/style.css">
    <link rel="stylesheet" href="resources/css/font-awesome.min.css">
    <link rel="stylesheet" href="resources/css/animate.css">
    <link rel="stylesheet" href="resources/css/bootstrap.min.css">
    <link rel="stylesheet" href="resources/css/classy-nav.css">
    <link rel="stylesheet" href="resources/css/magnific-popup.css">
    <link rel="stylesheet" href="resources/css/nice-select.css">
<link rel="stylesheet" href="resources/css/owl.carousel.min.css">
</head>
<body>

	<!-- Preloader -->
    <div class="preloader d-flex align-items-center justify-content-center">
        <div class="preloader-content">
            <h3>Cooking in progress..</h3>
            <div id="cooking">
                <div class="bubble"></div>
                <div class="bubble"></div>
                <div class="bubble"></div>
                <div class="bubble"></div>
                <div class="bubble"></div>
                <div id="area">
                    <div id="sides">
                        <div id="pan"></div>
                        <div id="handle"></div>
                    </div>
                    <div id="pancake">
                        <div id="pastry"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <header class="header-area">

        <!-- Top Header Area -->
        <div class="top-header-area bg-img bg-overlay" style="background-image: url(resources/img/bg-img/header.jpg);">
            <div class="container h-100">
                <div class="row h-100 align-items-center justify-content-between">
                    <div class="col-12 col-sm-6">
                        <!-- Top Social Info -->
                        <div class="top-social-info">
                            <a href="#" data-toggle="tooltip" data-placement="bottom" title="Pinterest"><i class="fa fa-pinterest" aria-hidden="true"></i></a>
                            <a href="#" data-toggle="tooltip" data-placement="bottom" title="Facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                            <a href="#" data-toggle="tooltip" data-placement="bottom" title="Twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                            <a href="#" data-toggle="tooltip" data-placement="bottom" title="Dribbble"><i class="fa fa-dribbble" aria-hidden="true"></i></a>
                            <a href="#" data-toggle="tooltip" data-placement="bottom" title="Behance"><i class="fa fa-behance" aria-hidden="true"></i></a>
                            <a href="#" data-toggle="tooltip" data-placement="bottom" title="Linkedin"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-lg-5 col-xl-4">
                        
                    </div>
                </div>
            </div>
        </div>


        <!-- Navbar Area -->
        <div class="bueno-main-menu" id="sticker">
            <div class="classy-nav-container breakpoint-off">
                <div class="container">
                    <!-- Menu -->
                    <nav class="classy-navbar justify-content-between" id="buenoNav">

                        <!-- Toggler -->
                        <div id="toggler"><img src="" alt=""></div>

                        <!-- Navbar Toggler -->
                        <div class="classy-navbar-toggler">
                            <span class="navbarToggler"><span></span><span></span><span></span></span>
                        </div>

                        <!-- Menu -->
                        <div class="classy-menu">

                            <!-- Close Button -->
                            <div class="classycloseIcon">
                                <div class="cross-wrap"><span class="top"></span><span class="bottom"></span></div>
                            </div>

                            <!-- Nav Start -->
                            <div class="classynav">
                                <ul>
                                    <li><a href="/spring">Home</a></li>
                                    <li><a href="#">Category</a>
                                        <ul class="dropdown">
                                        	<c:forEach items="${listCategory }" var="listCategory">
                                            <li><a href="product?id=<c:out value="${listCategory.getId() }"/>"><c:out value="${listCategory.getName() }"></c:out></a></li>
                                            </c:forEach>
                                        </ul>
                                    </li>
                                    <li><a href="#">About Us</a></li>
                                    <li><a href="single-post.html">Blog</a></li>
                                    <li><a href="contact.html">Contact</a></li>

							<li><a href="#">Account</a>
								<ul class="dropdown">
									<c:choose>
										<c:when test="${name != null }">
											<div class="email">
												<ul>
													<li><c:out value="${name }" /></li>
													<li><a href="/spring/logout">Logout</a></li>
												</ul>
											</div>
										</c:when>
										<c:otherwise>
													<li><a href="login">Login</a></li>
													<li><a href="register">Register</a></li>
									
										</c:otherwise>
									</c:choose>
									
								</ul></li>
						</ul>
                            </div>
                            <!-- Nav End -->

                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </header>
    
    <div class="cart1">
    	<a href="/spring/basecart"><i class="fa fa-shopping-cart" aria-hidden="true"></i></a>
    	
    
			<c:if test="${sizeCart > 0 || sizeCart != null }">
				<div class="sizecart">
					<p>${sizeCart }</p>
				</div>
			</c:if>    
			
    </div>
    
  	<div class="chitiet">
    	<h4>Chi tiết sản phẩm</h4>
    </div>
    <div class="container">
    	<div class="row">
    		<div class="col-sm-12 col-md-3">
    			<img alt="" src="resources<c:out value="${product.getImage() }" />">
    		</div>
    		<div class="col-sm-12 col-md-6">
    			<p id="name" data-id="<c:out value="${product.getId() }" />"><c:out value="${product.getName() }" /></p>
    			<p id="price" data-price="<c:out value="${product.getPrice() }" />"><c:out value="${product.getPrice() }" /> VNĐ</p>
    			<table class="table">
    				<thead>
    				<tr>
    					<th>Màu</th>
    					<th></th>
    				</tr>
    				</thead>
    				<tbody>
    				<c:forEach items="${product.getDetailProducts() }" var="detailProduct">
    				<tr>
    					<td class="color" data-iddetail="<c:out value="${detailProduct.getId()}" />" data-color="<c:out value="${detailProduct.getColor().getId() }" />"><c:out value="${detailProduct.getColor().getName() }" /></td>
    					<td><button class="btn btn-primary cart">Giỏ hàng</button></td>
    				</tr>
    				</c:forEach>
    				</tbody>
    			</table>
    		</div>
    		<div class="col-sm-12 col-md-3">
    			<h5>Thông tin mô tả</h5>
    			<p><c:out value="${product.getDescription() }" /></p>
    		</div>
    	</div>
    </div>

	
	 <!-- ##### All Javascript Script ##### -->
    <!-- jQuery-2.2.4 js -->
    <script src="resources/js/jquery/jquery-2.2.4.min.js"></script>
    <!-- Popper js -->
    <script src="resources/js/bootstrap/popper.min.js"></script>
    <!-- Bootstrap js -->
    <script src="resources/js/bootstrap/bootstrap.min.js"></script>
    <!-- All Plugins js -->
    <script src="resources/js/plugins/plugins.js"></script>
    <!-- Active js -->
    <script src="resources/js/active.js"></script>
</body>
</html>