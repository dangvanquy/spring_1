<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <title>Bueno - Food Blog HTML Template</title>

    <link rel="icon" href="resources/img/core-img/favicon.ico">

    <!-- Stylesheet -->
    <link rel="stylesheet" href="resources/css/style.css">
    <link rel="stylesheet" href="resources/css/font-awesome.min.css">
    <link rel="stylesheet" href="resources/css/animate.css">
    <link rel="stylesheet" href="resources/css/bootstrap.min.css">
    <link rel="stylesheet" href="resources/css/classy-nav.css">
    <link rel="stylesheet" href="resources/css/magnific-popup.css">
    <link rel="stylesheet" href="resources/css/nice-select.css">
    <link rel="stylesheet" href="resources/css/owl.carousel.min.css">
    
</head>

<body>
    <!-- Preloader -->
    <div class="preloader d-flex align-items-center justify-content-center">
        <div class="preloader-content">
            <h3>Cooking in progress..</h3>
            <div id="cooking">
                <div class="bubble"></div>
                <div class="bubble"></div>
                <div class="bubble"></div>
                <div class="bubble"></div>
                <div class="bubble"></div>
                <div id="area">
                    <div id="sides">
                        <div id="pan"></div>
                        <div id="handle"></div>
                    </div>
                    <div id="pancake">
                        <div id="pastry"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- ##### Header Area Start ##### -->
    <header class="header-area">

        <!-- Top Header Area -->
        <div class="top-header-area bg-img bg-overlay" style="background-image: url(resources/img/bg-img/header.jpg);">
            <div class="container h-100">
                <div class="row h-100 align-items-center justify-content-between">
                    <div class="col-12 col-sm-6">
                        <!-- Top Social Info -->
                        <div class="top-social-info">
                            <a href="#" data-toggle="tooltip" data-placement="bottom" title="Pinterest"><i class="fa fa-pinterest" aria-hidden="true"></i></a>
                            <a href="#" data-toggle="tooltip" data-placement="bottom" title="Facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                            <a href="#" data-toggle="tooltip" data-placement="bottom" title="Twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                            <a href="#" data-toggle="tooltip" data-placement="bottom" title="Dribbble"><i class="fa fa-dribbble" aria-hidden="true"></i></a>
                            <a href="#" data-toggle="tooltip" data-placement="bottom" title="Behance"><i class="fa fa-behance" aria-hidden="true"></i></a>
                            <a href="#" data-toggle="tooltip" data-placement="bottom" title="Linkedin"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-lg-5 col-xl-4">
                        <!-- Top Search Area -->
                        <div class="top-search-area">
                            <form action="#" method="post">
                                <input type="search" name="top-search" id="topSearch" placeholder="Search">
                                <button type="submit" class="btn"><i class="fa fa-search"></i></button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Logo Area -->
        <div class="logo-area">
            <a href="index.html"><img src="resources/img/core-img/logo.png" alt=""></a>
        </div>

        <!-- Navbar Area -->
        <div class="bueno-main-menu" id="sticker">
            <div class="classy-nav-container breakpoint-off">
                <div class="container">
                    <!-- Menu -->
                    <nav class="classy-navbar justify-content-between" id="buenoNav">

                        <!-- Toggler -->
                        <div id="toggler"><img src="resources/img/core-img/toggler.png" alt=""></div>

                        <!-- Navbar Toggler -->
                        <div class="classy-navbar-toggler">
                            <span class="navbarToggler"><span></span><span></span><span></span></span>
                        </div>

                        <!-- Menu -->
                        <div class="classy-menu">

                            <!-- Close Button -->
                            <div class="classycloseIcon">
                                <div class="cross-wrap"><span class="top"></span><span class="bottom"></span></div>
                            </div>

                            <!-- Nav Start -->
                            <div class="classynav">
                                <ul>
                                    <li><a href="/spring">Home</a></li>
                                    <li><a href="#">Category</a>
                                        <ul class="dropdown">
                                        	<c:forEach items="${listCategory }" var="listCategory">
                                            <li><a href="product?id=<c:out value="${listCategory.getId() }"/>"><c:out value="${listCategory.getName() }"></c:out></a></li>
                                            </c:forEach>
                                        </ul>
                                    </li>
                                    <li><a href="#">About Us</a></li>
                                    <li><a href="single-post.html">Blog</a></li>
                                    <li><a href="contact.html">Contact</a></li>

							<li><a href="#">Account</a>
								<ul class="dropdown">
									<c:choose>
										<c:when test="${name != null }">
											<div class="email">
												<ul>
													<li><c:out value="${name }" /></li>
													<li><a href="/spring/logout">Logout</a></li>
												</ul>
											</div>
										</c:when>
										<c:otherwise>
													<li><a href="login">Login</a></li>
													<li><a href="register">Register</a></li>
									
										</c:otherwise>
									</c:choose>
									
								</ul></li>
						</ul>
                            </div>
                            <!-- Nav End -->

                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </header>
    <!-- ##### Header Area End ##### -->

	<ul>
	<c:forEach items="${customer.getCustomerRoles() }" var="customerRole">
		<li><c:out value="${customerRole.getRole().getName() }" /></li>
	</c:forEach>
	</ul>
	<!-- ##### Treading Post Area Start ##### -->
    <div class="treading-post-area" id="treadingPost">
        <div class="close-icon">
            <i class="fa fa-times"></i>
        </div>

        <h4>Sản phẩm mới</h4>

        <!-- Single Blog Post -->
        <div class="single-blog-post style-1 d-flex flex-wrap mb-30">
            <!-- Blog Thumbnail -->
            <c:forEach items="${listProductNew }" var="listProductNew">
            <div class="blog-thumbnail">
                <img src="resources<c:out value="${listProductNew.getImage() }" />" alt="">
            </div>
            <!-- Blog Content -->
            <div class="blog-content">
                <a href="#" class="post-tag"></a>
                <a href="detail?id=<c:out value="${listProductNew.getId() }" />" class="post-title"><c:out value="${listProductNew.getName() }"/></a>
                <p><c:out value="${listProductNew.getPrice() }" /> VNĐ</p>
            </div>
            </c:forEach>
        </div>


       
    </div>
    <!-- ##### Treading Post Area End ##### -->

    <!-- ##### Hero Area Start ##### -->
    <div class="hero-area">
        <!-- Hero Post Slides -->
        <div class="hero-post-slides owl-carousel">
            <!-- Single Slide -->
            
            <c:forEach items="${listSlide }" var="listSlide">
            <div class="single-slide">
                <!-- Blog Thumbnail -->
                <div class="blog-thumbnail">
                    <a href="#"><img src="resources<c:out value="${listSlide.getImage() }"/>" alt=""></a>
                </div>    
            </div>
			</c:forEach>
        </div>
    </div>
    <!-- ##### Hero Area End ##### -->

    <!-- ##### Catagory Area Start ##### -->
    <div class="post-catagory section-padding-100-0 mb-70">
        <div class="container">
            <div class="row justify-content-center">
                <!-- Single Post Catagory -->
                
                <c:forEach items="${listProductBest }" var="listProductBest">
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="single-post-catagory mb-30">
                     <a href="detail?id=<c:out value="${listProductBest.getId() }" />">
                        <img src="resources<c:out value="${listProductBest.getImage() }" />" alt="">
                        <div class="content">
                        <h4><c:out value="${listProductBest.getName() }" /></h4>
                        <p><c:out value="${listProductBest.getPrice() }" /> VNĐ</p>
                        </div>
                   	</a>
                    </div>
                </div>
				</c:forEach>
               

                
            </div>
        </div>
    </div>
    <!-- ##### Catagory Area End ##### -->

   

    <!-- ##### Posts Area End ##### -->
    <div class="bueno-post-area mb-70">
        <div class="container">
            <div class="row justify-content-center">
                <!-- Post Area -->
                <div class="col-12 col-lg-8 col-xl-9">
                    <!-- Single Blog Post -->
                    <c:forEach items="${listProduct1 }" var="listProduct1">
                    <div class="single-blog-post style-1 d-flex flex-wrap mb-30">
                        <!-- Blog Thumbnail -->
                        <div class="blog-thumbnail">
                            <img src="resources<c:out value="${listProduct1.getImage() }" />" alt="">
                        </div>
                        <!-- Blog Content -->
                        <div class="blog-content">
                            <a href="<c:out value="${listProduct1.getId() }" />" class="post-title"><c:out value="${listProduct1.getName() }" /></a>
                            <div class="post-meta">
                                <p><c:out value="${listProduct1.getPrice() }" /> VNĐ</p>
                            </div>
                            <p><c:out value="${listProduct1.getDescription() }" /></p>
                       	</div>
                    </div>
                    </c:forEach>
                </div>

                <!-- Sidebar Area -->
                <div class="col-12 col-sm-9 col-md-6 col-lg-4 col-xl-3">
                    <div class="sidebar-area">

                        <!-- Single Widget Area -->
                        <div class="single-widget-area add-widget mb-30">
                            <img src="resources/img/bg-img/add.png" alt="">
                        </div>

                        <!-- Single Widget Area -->
                        <div class="single-widget-area post-widget">

                            <!-- Single Post Area -->
                           
                                <!-- Blog Thumbnail -->
                                <c:forEach items="${listProductDetail }" var="listProductDetail">
                                 <div class="single-post-area d-flex">
                                <div class="blog-thumbnail">
                                    <a href="detail?id=<c:out value="${listProductDetail.getId() }" />"><img src="resources<c:out value="${listProductDetail.getImage() }" />" alt=""></a>
                                </div>
                                <!-- Blog Content -->
                                <div class="blog-content">
                                    <a href="detail?id=<c:out value="${listProductDetail.getId() }" />" class="post-title"><c:out value="${listProductDetail.getName() }" /></a>
                                    <div class="post-meta">
                                        <c:out value="${listProductDetail.getPrice() }" /> VNĐ
                                    </div>
                                </div>
                                </div>
                                </c:forEach>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ##### Posts Area End ##### -->

    <div class="footer">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-5">
                    <!-- Copywrite Text -->
                    <p class="copywrite-text"><a href="#"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
                </div>
                <div class="col-12 col-sm-7">
                    
                </div>
            </div>
        </div>
        </div>

    <!-- ##### All Javascript Script ##### -->
    <!-- jQuery-2.2.4 js -->
    <script src="resources/js/jquery/jquery-2.2.4.min.js"></script>
    <!-- Popper js -->
    <script src="resources/js/bootstrap/popper.min.js"></script>
    <!-- Bootstrap js -->
    <script src="resources/js/bootstrap/bootstrap.min.js"></script>
    <!-- All Plugins js -->
    <script src="resources/js/plugins/plugins.js"></script>
    <!-- Active js -->
    <script src="resources/js/active.js"></script>
</body>

</html>