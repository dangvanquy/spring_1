<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>cart</title>

<link rel="stylesheet" href="resources/css/style.css">
<link rel="stylesheet" href="resources/css/font-awesome.min.css">
<link rel="stylesheet" href="resources/css/animate.css">
<link rel="stylesheet" href="resources/css/bootstrap.min.css">
<link rel="stylesheet" href="resources/css/classy-nav.css">
<link rel="stylesheet" href="resources/css/magnific-popup.css">
<link rel="stylesheet" href="resources/css/nice-select.css">
<link rel="stylesheet" href="resources/css/owl.carousel.min.css">
</head>
<body>
	<!-- Preloader -->
	<div class="preloader d-flex align-items-center justify-content-center">
		<div class="preloader-content">
			<h3>Cooking in progress..</h3>
			<div id="cooking">
				<div class="bubble"></div>
				<div class="bubble"></div>
				<div class="bubble"></div>
				<div class="bubble"></div>
				<div class="bubble"></div>
				<div id="area">
					<div id="sides">
						<div id="pan"></div>
						<div id="handle"></div>
					</div>
					<div id="pancake">
						<div id="pastry"></div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<header class="header-area">

		<!-- Top Header Area -->
		<div class="top-header-area bg-img bg-overlay"
			style="background-image: url(resources/img/bg-img/header.jpg);">
			<div class="container h-100">
				<div class="row h-100 align-items-center justify-content-between">
					<div class="col-12 col-sm-6">
						<!-- Top Social Info -->
						<div class="top-social-info">
							<a href="#" data-toggle="tooltip" data-placement="bottom"
								title="Pinterest"><i class="fa fa-pinterest"
								aria-hidden="true"></i></a> <a href="#" data-toggle="tooltip"
								data-placement="bottom" title="Facebook"><i
								class="fa fa-facebook" aria-hidden="true"></i></a> <a href="#"
								data-toggle="tooltip" data-placement="bottom" title="Twitter"><i
								class="fa fa-twitter" aria-hidden="true"></i></a> <a href="#"
								data-toggle="tooltip" data-placement="bottom" title="Dribbble"><i
								class="fa fa-dribbble" aria-hidden="true"></i></a> <a href="#"
								data-toggle="tooltip" data-placement="bottom" title="Behance"><i
								class="fa fa-behance" aria-hidden="true"></i></a> <a href="#"
								data-toggle="tooltip" data-placement="bottom" title="Linkedin"><i
								class="fa fa-linkedin" aria-hidden="true"></i></a>
						</div>
					</div>
					<div class="col-12 col-sm-6 col-lg-5 col-xl-4"></div>
				</div>
			</div>
		</div>


		<!-- Navbar Area -->
		<div class="bueno-main-menu" id="sticker">
			<div class="classy-nav-container breakpoint-off">
				<div class="container">
					<!-- Menu -->
					<nav class="classy-navbar justify-content-between" id="buenoNav">

						<!-- Toggler -->
						<div id="toggler">
							<img src="" alt="">
						</div>

						<!-- Navbar Toggler -->
						<div class="classy-navbar-toggler">
							<span class="navbarToggler"><span></span><span></span><span></span></span>
						</div>

						<!-- Menu -->
						<div class="classy-menu">

							<!-- Close Button -->
							<div class="classycloseIcon">
								<div class="cross-wrap">
									<span class="top"></span><span class="bottom"></span>
								</div>
							</div>

							<!-- Nav Start -->
							<div class="classynav">
								<ul>
									<li><a href="/spring">Home</a></li>
									<li><a href="#">Category</a>
                                        <ul class="dropdown">
                                        	<c:forEach items="${listCategory }" var="listCategory">
                                            <li><a href="product?id=<c:out value="${listCategory.getId() }"/>"><c:out value="${listCategory.getName() }"></c:out></a></li>
                                            </c:forEach>
                                        </ul>
                                    </li>
									<li><a href="#">About Us</a></li>
									<li><a href="single-post.html">Blog</a></li>
									<li><a href="contact.html">Contact</a></li>

									<li><a href="#">Account</a>
										<ul class="dropdown">
											<c:choose>
												<c:when test="${name != null }">
													<div class="email">
														<ul>
															<li><c:out value="${name }" /></li>
															<li><a href="/spring/logout">Logout</a></li>
														</ul>
													</div>
												</c:when>
												<c:otherwise>
													<li><a href="login">Login</a></li>
													<li><a href="register">Register</a></li>

												</c:otherwise>
											</c:choose>

										</ul></li>
								</ul>
							</div>
							<!-- Nav End -->

						</div>
					</nav>
				</div>
			</div>
		</div>
	</header>

	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<div class="carttitle">
					<h4>Thông tin giỏ hàng</h4>
				</div>
				<table class="table">
					<thead>
						<tr>
							<td>Tên</td>
							<td>Giá</td>
							<td>Màu</td>
							<td>Số lượng</td>
							<td></td>
						</tr>
					</thead>
					<tbody>

						<c:forEach items="${listCart }" var="listCart">
							<tr>
								<td class="name"
								data-id="<c:out value= "${listCart.getId() }" />"><c:out value="${listCart.getName() }" /></td>
								<td class="price" data-value="<c:out value="${listCart.getPrice() }" />"><c:out value="${listCart.getPrice() }" /> VNĐ</td>
								<td class="color" data-color="<c:out value="${listCart.getIdcolor() }" />"><c:out value="${listCart.getColor() }" /></td>
								<td><input class="quantity" type="number" name="quantity" min="1" value="<c:out value="${listCart.getQuantity() }" />"/></td>
								<td><button class="btn btn-danger delete">Xóa</button></td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
				
				
					<span>
					<h4>Tổng tiền</h4>
					<h4 id="totalprice">0 VNĐ</h4>
					</span>
				
			</div>
			<div class="col-md-6">
				<div class="carttitlecustomer">
				<h4>Thông tin khách hàng</h4>
				</div>
				<form action="" method="post">
					<div class="form-group">
						<label for="exampleInputEmail1">Họ tên</label> <input type="text"
							class="form-control" name="name" id="exampleInputEmail1"
							placeholder="Nhập tên"> <small
							class="form-text text-muted"></small>
					</div>
					<div class="form-group">
						<label for="exampleInputPassword1">Điện thoại liên lạc</label> <input
							type="text" class="form-control" name="phone"
							placeholder="Nhập số điện thoại">
					</div>

					<div class="radio">
						<label> <input type="radio" name="choose"
							checked="checked" value="Nhận hàng tại nhà" />Nhận hàng tại nhà
						</label>
					</div>

					<div class="radio">
						<label> <input type="radio" name="choose" value="Nhận hàng tại
							cửa hàng gần nhất" />Nhận hàng tại
							cửa hàng gần nhất
						</label>
					</div>

					<div class="form-group">
						<label for="exampleInputEmail1">Địa chỉ</label> <input type="text"
							class="form-control" name="address" placeholder="Nhập địa chỉ">
						<small class="form-text text-muted"></small>
					</div>


					<div class="form-group">
						<label for="exampleFormControlTextarea1">Ghi chú</label>
						<textarea class="form-control" id="exampleFormControlTextarea1" name="noted"
							rows="3"></textarea>
					</div>

					<button type="submit" class="btn btn-primary">Submit</button>
				</form>
			</div>
		</div>
	</div>
	
	<!-- ##### Footer Area Start ##### -->
    	<div class="footer">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-5">
                    <!-- Copywrite Text -->
                    <p class="copywrite-text"><a href="#"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
                </div>
                <div class="col-12 col-sm-7">
                    
                </div>
            </div>
        </div>
        </div>
  
    <!-- ##### Footer Area Start ##### -->
	<!-- ##### All Javascript Script ##### -->
	<!-- jQuery-2.2.4 js -->
	<script src="resources/js/jquery/jquery-2.2.4.min.js"></script>
	<!-- Popper js -->
	<script src="resources/js/bootstrap/popper.min.js"></script>
	<!-- Bootstrap js -->
	<script src="resources/js/bootstrap/bootstrap.min.js"></script>
	<!-- All Plugins js -->
	<script src="resources/js/plugins/plugins.js"></script>
	<!-- Active js -->
	<script src="resources/js/active.js"></script>
</body>
</html>