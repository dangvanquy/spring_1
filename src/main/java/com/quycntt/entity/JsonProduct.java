package com.quycntt.entity;

import java.util.List;

public class JsonProduct {
	private int id;
	private String name;
	private String price;
	private String image;
	private String description;
	private Category category;
	private List<DetailProduct> detailProducts;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Category getCategory() {
		return category;
	}
	public void setCategory(Category category) {
		this.category = category;
	}
	public List<DetailProduct> getDetailProducts() {
		return detailProducts;
	}
	public void setDetailProducts(List<DetailProduct> detailProducts) {
		this.detailProducts = detailProducts;
	}
}
