package com.quycntt.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;

@Entity(name="invoicedetail")
public class InvoiceDetail {
	
	@EmbeddedId
	InvoiceDetailId invoiceDetailId;
	
	@Column(name="price")
	private String price;
	
	@Column(name="quantity")
	private int quanity;

	public InvoiceDetailId getInvoiceDetailId() {
		return invoiceDetailId;
	}

	public void setInvoiceDetailId(InvoiceDetailId invoiceDetailId) {
		this.invoiceDetailId = invoiceDetailId;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public int getQuanity() {
		return quanity;
	}

	public void setQuanity(int quanity) {
		this.quanity = quanity;
	}
}
