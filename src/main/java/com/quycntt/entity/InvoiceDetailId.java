package com.quycntt.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class InvoiceDetailId implements Serializable{
	@Column(name="idinvoice")
	private int idinvoice;
	
	@Column(name="iddetail")
	private int iddetail;
	public int getIdinvoice() {
		return idinvoice;
	}
	public void setIdinvoice(int idinvoice) {
		this.idinvoice = idinvoice;
	}
	public int getIddetail() {
		return iddetail;
	}
	public void setIddetail(int iddetail) {
		this.iddetail = iddetail;
	}
}
