package com.quycntt.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.quycntt.dao.CategoryDao;
import com.quycntt.daoimp.CategoryDaoImp;
import com.quycntt.entity.Category;

@Service
public class CategoryService implements CategoryDao{

	@Autowired
	CategoryDaoImp categoryDaoImp;
	
	public List<Category> getListCategory() {
		return categoryDaoImp.getListCategory();
	}

	public Category getCategory(int id) {
		return categoryDaoImp.getCategory(id);
	}
}
