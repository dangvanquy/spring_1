package com.quycntt.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.quycntt.dao.InvoiceDetailDao;
import com.quycntt.daoimp.InvoiceDetailDaoImp;
import com.quycntt.entity.InvoiceDetail;

@Service
public class InvoiceDetailService implements InvoiceDetailDao{

	@Autowired
	private InvoiceDetailDaoImp invoiceDetailDaoImp;
	
	public boolean addInvoiceDetail(InvoiceDetail invoiceDetail) {
		return invoiceDetailDaoImp.addInvoiceDetail(invoiceDetail);
	}
}
