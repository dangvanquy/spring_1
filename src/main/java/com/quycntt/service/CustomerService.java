package com.quycntt.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.quycntt.dao.CustomerDao;
import com.quycntt.daoimp.CustomerDaoImp;
import com.quycntt.entity.Customer;

@Service
public class CustomerService implements CustomerDao{

	@Autowired
	private CustomerDaoImp customerDaoImp;
	
	public Customer checkLogin(String email, String password) {
		return customerDaoImp.checkLogin(email, password);
	}

	public boolean checkRegister(String name, String phone, String address, String email, String password) {
		return customerDaoImp.checkRegister(name, phone, address, email, password);
	}

	public Customer findByEmail(String email) {
		return customerDaoImp.findByEmail(email);
	}
}
