package com.quycntt.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.quycntt.dao.InvoiceDao;
import com.quycntt.daoimp.InvoiceDaoImp;
import com.quycntt.entity.Invoice;

@Service
public class InvoiceService implements InvoiceDao{
	
	@Autowired
	InvoiceDaoImp invoiceDaoImp;
	
	public int addInvoice(Invoice invoice) {
		return invoiceDaoImp.addInvoice(invoice);
	}
}
