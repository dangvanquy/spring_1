package com.quycntt.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.quycntt.dao.ProductDao;
import com.quycntt.daoimp.ProductDaoImp;
import com.quycntt.entity.Product;

@Service
public class ProductService implements ProductDao{

	@Autowired
	private ProductDaoImp productDaoImp;
	
	public List<Product> getListBest() {
		return productDaoImp.getListBest();
	}

	public List<Product> geteListNew() {
		return productDaoImp.geteListNew();
	}

	public Product getProduct(int id) {
		return productDaoImp.getProduct(id);
	}

	public List<Product> getListProduct(int id) {
		return productDaoImp.getListProduct(id);
	}

	public List<Product> getListProductDetail(int number) {
		return productDaoImp.getListProductDetail(number);
	}

	public List<Product> getListProduct1() {
		return productDaoImp.getListProduct1();
	}

	public List<Product> getListProduct() {
		return productDaoImp.getListProduct();
	}

	public void deleteProduct(int id) {
		productDaoImp.deleteProduct(id);
	}

	public void addProduct(Product product) {
		productDaoImp.addProduct(product);
	}
}
