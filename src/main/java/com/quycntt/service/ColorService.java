package com.quycntt.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.quycntt.dao.ColorDao;
import com.quycntt.daoimp.ColorDaoImp;
import com.quycntt.entity.Color;

@Service
public class ColorService implements ColorDao{

	@Autowired
	private ColorDaoImp colorDaoImp;
	
	public List<Color> getListColor() {
		return colorDaoImp.getListColor();
	}
}
