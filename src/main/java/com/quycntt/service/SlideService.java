package com.quycntt.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.quycntt.dao.SlideDao;
import com.quycntt.daoimp.SlideDaoImp;
import com.quycntt.entity.Slide;

@Service
public class SlideService implements SlideDao{

	@Autowired
	SlideDaoImp slideDaoImp;
	
	public List<Slide> getSlide() {
		return slideDaoImp.getSlide();
	}
	
}
