package com.quycntt.daoimp;

import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Repository;

import com.quycntt.dao.InvoiceDao;
import com.quycntt.dao.InvoiceDetailDao;
import com.quycntt.entity.Invoice;
import com.quycntt.entity.InvoiceDetail;
import com.quycntt.entity.InvoiceDetailId;

@Repository
@Scope(proxyMode=ScopedProxyMode.TARGET_CLASS)
public class InvoiceDetailDaoImp implements InvoiceDetailDao{
	
	@Autowired
	SessionFactory sessionFactory;
	
	@Transactional
	public boolean addInvoiceDetail(InvoiceDetail invoiceDetail) {
		Session session = sessionFactory.getCurrentSession();
		
		InvoiceDetailId invoiceDetailId = (InvoiceDetailId) session.save(invoiceDetail);
		if (invoiceDetailId != null) {
			return true;
		} else {
			return false;
		}
	}

}
