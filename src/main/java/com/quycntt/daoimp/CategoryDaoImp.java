package com.quycntt.daoimp;

import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Repository;

import com.quycntt.dao.CategoryDao;
import com.quycntt.entity.Category;

@Repository
@Scope(proxyMode=ScopedProxyMode.TARGET_CLASS)
public class CategoryDaoImp implements CategoryDao{

	@Autowired
	SessionFactory sessionFactory;
	
	@Transactional
	public List<Category> getListCategory() {
		Session session = sessionFactory.getCurrentSession();
		String sql = "from category";
		List<Category> listCategory = session.createQuery(sql).setFirstResult(0).setMaxResults(4).getResultList();
		return listCategory;
	}

	@Transactional
	public Category getCategory(int id) {
		Session session = sessionFactory.getCurrentSession();
		String sql = "from category where id =" + id;
		Category category = (Category) session.createQuery(sql).getSingleResult();
		return category;
	}

}
