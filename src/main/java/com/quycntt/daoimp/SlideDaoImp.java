package com.quycntt.daoimp;

import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Repository;

import com.quycntt.dao.SlideDao;
import com.quycntt.entity.Slide;

@Repository
@Scope(proxyMode=ScopedProxyMode.TARGET_CLASS)
public class SlideDaoImp implements SlideDao{

	@Autowired
	SessionFactory sessionFactory;

	@Transactional
	public List<Slide> getSlide() {
		Session session = sessionFactory.getCurrentSession();
		String sql = "from slide";
		List<Slide> listSlide = session.createQuery(sql).getResultList();
		return listSlide;
	}
}
