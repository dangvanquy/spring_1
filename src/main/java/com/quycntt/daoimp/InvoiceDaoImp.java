package com.quycntt.daoimp;

import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Repository;

import com.quycntt.dao.InvoiceDao;
import com.quycntt.entity.Invoice;

@Repository
@Scope(proxyMode=ScopedProxyMode.TARGET_CLASS)
public class InvoiceDaoImp implements InvoiceDao{

	@Autowired
	SessionFactory sessionFactory;
	
	@Transactional
	public int addInvoice(Invoice invoice) {
		Session session = sessionFactory.getCurrentSession();
		int id = (Integer) session.save(invoice);
		if (id > 0) {
			return id;
		} else {
			return 0;
		}
	}
	
}
