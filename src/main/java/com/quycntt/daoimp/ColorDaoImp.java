package com.quycntt.daoimp;

import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Repository;

import com.quycntt.dao.ColorDao;
import com.quycntt.entity.Color;

@Repository
@Scope(proxyMode=ScopedProxyMode.TARGET_CLASS)
public class ColorDaoImp implements ColorDao{
	
	@Autowired
	private SessionFactory sessionFactory;

	@Transactional
	public List<Color> getListColor() {
		Session session = sessionFactory.getCurrentSession();
		String sql = "from color";
		List<Color> listColor = session.createQuery(sql).getResultList();
		return listColor;
	}
}
