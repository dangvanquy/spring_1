package com.quycntt.daoimp;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Repository;

import com.quycntt.dao.ProductDao;
import com.quycntt.entity.DetailProduct;
import com.quycntt.entity.Invoice;
import com.quycntt.entity.InvoiceDetail;
import com.quycntt.entity.InvoiceDetailId;
import com.quycntt.entity.Product;

@Repository
@Scope(proxyMode=ScopedProxyMode.TARGET_CLASS)
public class ProductDaoImp implements ProductDao{
	
	@Autowired
	SessionFactory sessionFactory;

	@Transactional
	public List<Product> getListBest() {
		Session session = sessionFactory.getCurrentSession();
		String sql = "from product order by price";
		List<Product> listProduct = session.createQuery(sql).setFirstResult(0).setMaxResults(3).getResultList();
		return listProduct;
	}

	@Transactional
	public List<Product> geteListNew() {
		Session session = sessionFactory.getCurrentSession();
		String sql = "from product order by id";
		List<Product> listProductNew = session.createQuery(sql).setFetchSize(0).setMaxResults(9).getResultList();
		return listProductNew;
	}

	@Transactional
	public Product getProduct(int id) {
		Session session = sessionFactory.getCurrentSession();
		String sql = "from product p where p.id =" +id;
		Product product = (Product) session.createQuery(sql).getSingleResult();
		return product;
	}

	@Transactional
	public List<Product> getListProduct(int id) {
		Session session = sessionFactory.getCurrentSession();
		String sql = "from product where categoryid = " + id;
		List<Product> listProduct = session.createQuery(sql).getResultList();
		return listProduct;
	}

	@Transactional
	public List<Product> getListProductDetail(int number) {
		List<Product> listProduct = getProduct1(number);
		return listProduct;
	}

	@Transactional
	public List<Product> getListProduct1() {
		Session session = sessionFactory.getCurrentSession();
		String sql = "from product order by id desc";
		List<Product> listProduct1 = session.createQuery(sql).setFirstResult(0).setMaxResults(3).getResultList();
		return listProduct1;
	}

	@Transactional
	public List<Product> getListProduct() {
		Session session = sessionFactory.getCurrentSession();
		String sql = "from product";
		List<Product> listProduct = session.createQuery(sql).getResultList();
		return listProduct;
	}
	
	@Transactional
	private List<Product> getProduct1(int number) {
		Session session = sessionFactory.getCurrentSession();
		
		String sql = "from product";
		List<Product> listProduct = session.createQuery(sql).setFirstResult(number).setMaxResults(5).getResultList();
		return listProduct;
	}

	@Transactional
	public void deleteProduct(int id) {
		Session session = sessionFactory.getCurrentSession();
		Product product = session.get(Product.class, id);
		
		List<DetailProduct> detailProducts = product.getDetailProducts();
		for (DetailProduct detailProduct : detailProducts) {
			session.createQuery("delete invoicedetail where iddetail="+detailProduct.getId()).executeUpdate();			
		}
		
		session.createQuery("delete detail where productid="+id).executeUpdate();
		session.createQuery("delete product where id="+id).executeUpdate();
	}

	@Transactional
	public void addProduct(Product product) {
		Session session = sessionFactory.getCurrentSession();
		session.save(product);
	}
}
