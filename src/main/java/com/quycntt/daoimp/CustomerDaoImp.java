package com.quycntt.daoimp;

import org.springframework.transaction.annotation.Transactional;

import org.hibernate.*;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Repository;

import com.quycntt.dao.CustomerDao;
import com.quycntt.entity.Customer;

@Repository
@Scope(proxyMode=ScopedProxyMode.TARGET_CLASS)
public class CustomerDaoImp implements CustomerDao{

	@Autowired
	SessionFactory sessionFactory;
	
	@Transactional
	public Customer checkLogin(String email, String password) {
		Session session = sessionFactory.getCurrentSession(); 
		Customer customer = new Customer();
		try {
			String sql = "from Customer where email='"+email+"' and password='"+password+"'";
			customer = (Customer) session.createQuery(sql).getSingleResult();
			
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return customer;
	}

	@Transactional
	public boolean checkRegister(String name, String phone, String address, String email, String password) {
		Session session = sessionFactory.getCurrentSession();
		boolean check = false;
		
		try {
			Customer customer = new Customer();
			customer.setName(name);
			customer.setPhone(phone);
			customer.setAddress(address);
			customer.setEmail(email);
			customer.setPassword(password);
			
			int id = (Integer) session.save(customer);
			if (id > 0) {
				check = true;
			} else {
				check = false;
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return check;
	}

	@Transactional
	public Customer findByEmail(String email) {
		Session session = sessionFactory.getCurrentSession();
		Customer customer = null;
		try {
			String sql = "from Customer where email like :email";
		    Query query = session.createQuery(sql);
		    query.setString("email", "%" + email + "%");
			
		    customer = (Customer) query.uniqueResult();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
		return customer;
	}
}
