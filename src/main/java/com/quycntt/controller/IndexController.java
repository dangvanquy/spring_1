package com.quycntt.controller;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.quycntt.entity.Category;
import com.quycntt.entity.Product;
import com.quycntt.entity.Slide;
import com.quycntt.service.CategoryService;
import com.quycntt.service.ProductService;
import com.quycntt.service.SlideService;

@Controller
@RequestMapping("/")
public class IndexController {
	
	@Autowired
	private SlideService slideService;
	
	@Autowired
	private CategoryService categoryService;
	
	@Autowired
	private ProductService productService;
	
	@GetMapping
	public String index(ModelMap modelMap) {
		modelMap.addAttribute("listSlide", slideService.getSlide());
		modelMap.addAttribute("listCategory", categoryService.getListCategory());
		modelMap.addAttribute("listProductBest", productService.getListBest());
		modelMap.addAttribute("listProductNew", productService.geteListNew());
		modelMap.addAttribute("listProductDetail", productService.getListProductDetail(0));
		modelMap.addAttribute("listProduct1", productService.getListProduct1());
		return "index";
	}
	
	@GetMapping("/logout")
	public String logout(HttpSession httpSession, HttpServletRequest request,HttpServletResponse response) {
		if (httpSession.getAttribute("name") != null) {
			httpSession.removeAttribute("name");
		for (Cookie cookie : request.getCookies()) {
			if (cookie.getName().equalsIgnoreCase("email")) {
				cookie.setMaxAge(0);
				response.addCookie(cookie);
			}
			if (cookie.getName().equalsIgnoreCase("password")) {
				cookie.setMaxAge(0);
				response.addCookie(cookie);
			}
		}
		}
		return "redirect:/";
	}
}
