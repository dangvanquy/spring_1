package com.quycntt.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.quycntt.entity.Cart;
import com.quycntt.service.CategoryService;
import com.quycntt.service.ProductService;

@Controller
@RequestMapping("/detail")
@SessionAttributes("cartSession")
public class DetailController {
	
	@Autowired
	private ProductService productService;
	
	@Autowired
	CategoryService categoryService;

	@GetMapping
	public String detail(@RequestParam int id, ModelMap modelMap, HttpSession httpSession) {
		modelMap.addAttribute("product", productService.getProduct(id));
		modelMap.addAttribute("listCategory", categoryService.getListCategory());
		
		if (httpSession.getAttribute("name") != null) {
			String name = (String) httpSession.getAttribute("name");
			modelMap.addAttribute("name", name);
		}
		
		if (httpSession.getAttribute("cartSession") != null) {
			List<Cart> listCart = (List<Cart>) httpSession.getAttribute("cartSession");
			modelMap.addAttribute("sizeCart", listCart.size());
		}
		
		return "detail";
	}
}
