package com.quycntt.controller;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.persistence.PostLoad;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.quycntt.entity.Cart;
import com.quycntt.entity.Category;
import com.quycntt.entity.Color;
import com.quycntt.entity.DetailProduct;
import com.quycntt.entity.JsonProduct;
import com.quycntt.entity.Product;
import com.quycntt.service.ProductService;

@Controller
@RequestMapping("/base")
@SessionAttributes("cartSession")
public class BaseController {
	
	@Autowired
	private ProductService productService;
	
	List<Cart> carts = new ArrayList<Cart>();
	
	@GetMapping("/cart")
	public void cart(@RequestParam int id,@RequestParam int iddetail, @RequestParam String name, @RequestParam int idcolor, @RequestParam String namecolor, @RequestParam String price, HttpSession httpSession) {
		Cart cart = new Cart();
		cart.setId(id);
		cart.setIddetail(iddetail);
		cart.setName(name);
		cart.setIdcolor(idcolor);
		cart.setColor(namecolor);
		cart.setPrice(price);
		cart.setQuantity(1);
		
		if (httpSession.getAttribute("cartSession") == null) {
			carts.add(cart);
		} else {
			int idCheck = check(id, idcolor, httpSession);
			if (idCheck == -1) {
				carts.add(cart);
			} else {
				List<Cart> listCart = (List<Cart>) httpSession.getAttribute("cartSession");
				int quantityNew = listCart.get(idCheck).getQuantity() + 1;
				listCart.get(idCheck).setQuantity(quantityNew);
			}
		}
		
		httpSession.setAttribute("cartSession", carts);
	}
	
	@GetMapping("/delete")
	public void delete(@RequestParam int id, @RequestParam int idcolor, HttpSession httpSession) {
		List<Cart> listCart = (List<Cart>) httpSession.getAttribute("cartSession");
		int idCheck = check(id, idcolor, httpSession);
		if (idCheck != -1) {
			listCart.remove(idCheck);
		}
	}
	
	@GetMapping("/updatequantity")
	public void updateQuantity(@RequestParam int id, @RequestParam int idcolor, @RequestParam int quantity, HttpSession httpSession) {
		if (null != httpSession.getAttribute("cartSession")) {
			List<Cart> listCart = (List<Cart>) httpSession.getAttribute("cartSession");
			int idCheck = check(id, idcolor, httpSession);
			listCart.get(idCheck).setQuantity(quantity);
		}
	}
	
	@GetMapping("/paging")
	@ResponseBody
	public String paging(@RequestParam int beginNumber, ModelMap modelMap) {
		String html = "";
		List<Product> listProduct = productService.getListProductDetail(beginNumber);
		for (Product product : listProduct) {
			html += "<tr>";
			html += "<td>\n" + 
					"							<div class=\"checkbox\">\n" + 
					"								<label><input class=\"checkSingle\" type=\"checkbox\" value=\""+ product.getId() +"\"></label>\n" + 
					"							</div>\n" + 
					"						</td>";
			html += "<td>"+ product.getName() +"</td>";
			html += "<td>"+ product.getPrice() +"</td>";
			html += "<td>"+"<button class=\"btn btn-primary btn-update\" data-id=\""+ product.getId() +"\">Update</button>"+"</td>";
			html += "</tr>";
		}
		return html;
	}
	
	@GetMapping("/deleteproduct")
	@ResponseBody
	public String deleteProduct(@RequestParam int id) {
		productService.deleteProduct(id);
		return "true";
	}
	
	@Autowired
	ServletContext context;
	
	@PostMapping("/uploadfile")
	@ResponseBody 
	public String uploadFile(MultipartHttpServletRequest request) {
		String path_save = context.getRealPath("/resources/image/");
		Iterator<String> nameImages = request.getFileNames();
		MultipartFile multipartFile = request.getFile(nameImages.next());
		File file = new File(path_save + multipartFile.getOriginalFilename());
		
		try {
			multipartFile.transferTo(file);
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		System.out.println(path_save);
		return "true";
	}
	
	@PostMapping("/addproduct")
	@ResponseBody
	public String addProduct(@RequestParam String datajson) {
		ObjectMapper objectMapper = new ObjectMapper();
		JsonNode jsonProduct;
		try {
			jsonProduct = objectMapper.readTree(datajson);

			JsonNode jsonDetail = jsonProduct.get("detailproduct");
			List<DetailProduct> listDetail = new ArrayList<DetailProduct>();
			
			for (JsonNode jsonDetailValue : jsonDetail) {
				DetailProduct detailProduct = new DetailProduct();
				
				Color color = new Color();
				color.setId(jsonDetailValue.get("color").asInt());
				
				detailProduct.setColor(color);
				listDetail.add(detailProduct);
			}
			
			Category category = new Category();
			category.setId(jsonProduct.get("category").asInt());
			
			String name = jsonProduct.get("name").asText();
			String price = jsonProduct.get("price").asText();
			String description = jsonProduct.get("description").asText();
			String image = jsonProduct.get("image").asText();
			
			Product product = new Product();
			product.setCategory(category);
			product.setName(name);
			product.setPrice(price);
			product.setDescription(description);
			product.setImage(image);
			product.setDetailProducts(listDetail);
			
			productService.addProduct(product);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "true";
	}
	
	@PostMapping(path="/getdetailproduct", produces="application/json;charset=utf-8")
	@ResponseBody
	public JsonProduct getDetailProduct(@RequestParam int id) {
		Product product = productService.getProduct(id);
		JsonProduct jsonProduct = new JsonProduct();
		
		jsonProduct.setId(product.getId());
		jsonProduct.setName(product.getName());
		jsonProduct.setPrice(product.getPrice());
		jsonProduct.setImage(product.getImage());
		jsonProduct.setDescription(product.getDescription());
		
		Category category = new Category();
		category.setId(product.getCategory().getId());
		category.setName(product.getCategory().getName());
		
		jsonProduct.setCategory(category);
		
		List<DetailProduct> detailProducts = new ArrayList<DetailProduct>();
		
		for (DetailProduct detailProduct : product.getDetailProducts()) {
			DetailProduct detailProduct2 = new DetailProduct();
			detailProduct2.setId(detailProduct.getId());
			
			Color color = new Color();
			color.setId(detailProduct.getColor().getId());
			color.setName(detailProduct.getColor().getName());
			
			detailProduct2.setColor(color);
			
			detailProducts.add(detailProduct2);
		}
		
		jsonProduct.setDetailProducts(detailProducts);
		
		return jsonProduct;
	}
	
	private int check(int id, int idcolor, HttpSession httpSession) {
		List<Cart> listCart = (List<Cart>) httpSession.getAttribute("cartSession");
		for (int i=0;i<listCart.size();i++) {
			if (listCart.get(i).getId() == id && listCart.get(i).getIdcolor() == idcolor) {
				return i;
			}
		}
		return -1;
	}
}
