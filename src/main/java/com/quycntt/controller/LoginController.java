package com.quycntt.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.quycntt.entity.Customer;
import com.quycntt.service.CustomerService;

@Controller
@RequestMapping("/login")
public class LoginController {
	
	@Autowired
	private CustomerService customerService;
	
	@GetMapping
	public String login() {
		return "login";
	}
	
	@PostMapping
	public String loginAction(@RequestParam String email, @RequestParam String password, ModelMap modelMap, HttpSession httpSession) {
		if (customerService.checkLogin(email, password) != null) {
			Customer customer = customerService.findByEmail(email);
			String name = customer.getName();
			httpSession.setAttribute("name", name);
			modelMap.addAttribute("customer", customer);
			System.out.println(customer.getName());
			return "redirect:/";
		} else {
			modelMap.addAttribute("mess", "Email or Password error");
			return "login";
		}
	}
}
