package com.quycntt.controller;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.PostLoad;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.quycntt.entity.Cart;
import com.quycntt.entity.DetailProduct;
import com.quycntt.entity.Invoice;
import com.quycntt.entity.InvoiceDetail;
import com.quycntt.entity.InvoiceDetailId;
import com.quycntt.service.CategoryService;
import com.quycntt.service.InvoiceDetailService;
import com.quycntt.service.InvoiceService;

@Controller
@RequestMapping("/basecart")
@SessionAttributes("cartSession")
public class CartController {
	
	@Autowired
	CategoryService categoryService;
	
	@Autowired
	InvoiceService invoiceService;
	
	@Autowired
	InvoiceDetailService invoiceDetailService;
	
	@GetMapping
	public String baseCart(ModelMap modelMap, HttpSession httpSession) {
		modelMap.addAttribute("listCategory", categoryService.getListCategory());
		if (null != httpSession.getAttribute("cartSession")) {
			List<Cart> listCart = (List<Cart>) httpSession.getAttribute("cartSession");
			modelMap.addAttribute("listCart", listCart);
		}
		
		if (httpSession.getAttribute("name") != null) {
			String name = (String) httpSession.getAttribute("name");
			modelMap.addAttribute("name", name);
		}
		
		return "cart";
	}
	
	@PostMapping
	public String addInvoice(@RequestParam String name, @RequestParam String phone, @RequestParam String choose, @RequestParam String address, @RequestParam String noted, HttpSession httpSession) {
		Invoice invoice = new Invoice();
		invoice.setName(name);
		invoice.setPhone(phone);
		invoice.setDelivery(choose);
		invoice.setAddress(address);
		invoice.setNoted(noted);
		
		
		int id = invoiceService.addInvoice(invoice);
		
		if (null != httpSession.getAttribute("cartSession")) {
			List<Cart> listCart = (List<Cart>) httpSession.getAttribute("cartSession");
			for (Cart cart : listCart) {
				InvoiceDetailId invoiceDetailId = new InvoiceDetailId();
				invoiceDetailId.setIdinvoice(invoice.getId());
				invoiceDetailId.setIddetail(cart.getIddetail());
				
				InvoiceDetail invoiceDetail = new InvoiceDetail();
				invoiceDetail.setInvoiceDetailId(invoiceDetailId);
				invoiceDetail.setPrice(cart.getPrice());
				invoiceDetail.setQuanity(cart.getQuantity());
				
				invoiceDetailService.addInvoiceDetail(invoiceDetail);
			}
		}
		
		return "cart";
	}
}
