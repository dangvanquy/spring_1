package com.quycntt.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.quycntt.entity.Product;
import com.quycntt.service.CategoryService;
import com.quycntt.service.ColorService;
import com.quycntt.service.ProductService;

@Controller
@RequestMapping("/addproduct")
public class AddProductController {
	
	@Autowired
	CategoryService categoryService;
	
	@Autowired
	private ProductService productService;
	
	@Autowired
	private ColorService colorService;

	@GetMapping
	public String addProduct(ModelMap modelMap) {
		modelMap.addAttribute("listCategory", categoryService.getListCategory());
		modelMap.addAttribute("listProduct", productService.getListProductDetail(0));
		modelMap.addAttribute("listColor", colorService.getListColor());
		List<Product> listProduct = productService.getListProduct();
		double numberProduct = (double) listProduct.size();
	    double page = Math.ceil(numberProduct / 5);
		modelMap.addAttribute("page", page);
		System.out.println(page);
		return "addproduct";
	}
}
