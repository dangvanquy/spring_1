package com.quycntt.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.quycntt.entity.Customer;
import com.quycntt.service.CustomerService;

@Controller 
@RequestMapping("/register")
public class RegisterController {
	
	@Autowired
	private CustomerService customerService;
	
	@GetMapping
	public String register() {
		return "register";
	}
	
	@PostMapping
	public String registerAction(@RequestParam String name, @RequestParam String phone, @RequestParam String address, @RequestParam String email, @RequestParam String password, ModelMap modelMap) {
		boolean check = false;
		if (customerService.checkRegister(name, phone, address, email, password)) {
			return "redirect:login";
		} else {
			modelMap.addAttribute("mess", "Register error");
			return "register";
		}
	}
}
