package com.quycntt.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.quycntt.service.CategoryService;
import com.quycntt.service.ProductService;

@Controller
@RequestMapping("/product")
public class ProductController {
	
	@Autowired
	ProductService productService;
	
	@Autowired
	CategoryService categoryService;
	
	@GetMapping
	public String product(@RequestParam int id, ModelMap modelMap) {
		modelMap.addAttribute("listProduct", productService.getListProduct(id));
		modelMap.addAttribute("listCategory", categoryService.getListCategory());
		modelMap.addAttribute("category", categoryService.getCategory(id));
		return "product";
	}
}
