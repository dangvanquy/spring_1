package com.quycntt.dao;

import java.util.List;

import com.quycntt.entity.Color;

public interface ColorDao {
	List<Color> getListColor();
}
