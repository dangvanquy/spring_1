package com.quycntt.dao;

import java.util.List;

import com.quycntt.entity.Slide;

public interface SlideDao {
	List<Slide> getSlide();
}
