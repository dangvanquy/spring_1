package com.quycntt.dao;

import java.util.List;

import com.quycntt.entity.Product;

public interface ProductDao {
	List<Product> getListBest();
	List<Product> geteListNew();
	Product getProduct(int id);
	List<Product> getListProduct(int id);
	List<Product> getListProductDetail(int number);
	List<Product> getListProduct1();
	List<Product> getListProduct();
	void deleteProduct(int id);
	void addProduct(Product product);
}
