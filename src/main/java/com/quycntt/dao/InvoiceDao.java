package com.quycntt.dao;

import com.quycntt.entity.Invoice;

public interface InvoiceDao {
	int addInvoice(Invoice invoice);
}
