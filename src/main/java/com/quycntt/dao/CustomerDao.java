package com.quycntt.dao;

import com.quycntt.entity.Customer;

public interface CustomerDao {
	Customer checkLogin(String email, String password);
	boolean checkRegister(String name, String phone, String address, String email,String password);
	Customer findByEmail(String email);
}
