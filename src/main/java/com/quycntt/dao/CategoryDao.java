package com.quycntt.dao;

import java.util.List;

import com.quycntt.entity.Category;

public interface CategoryDao {
	List<Category> getListCategory();
	Category getCategory(int id);
}
